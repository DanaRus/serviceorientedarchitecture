package edu.ubb.soa.ui.service;

import edu.ubb.soa.ui.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class BookService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String RETRIEVE_ENDPOINT = "http://localhost:8082/books";
    private static final String SAVE_ENDPOINT = "http://localhost:8082/books/create";
    private static final String DELETE_ENDPOINT = "http://localhost:8082/books/delete";

    public List<Book> retrieve() {
        HttpEntity<Object> requestEntity = createRequestEntity();
        ResponseEntity<List<Book>> response = restTemplate.exchange(RETRIEVE_ENDPOINT, HttpMethod.GET, requestEntity,
                new ParameterizedTypeReference<List<Book>>() {
                });
        return response.getBody();
    }

    public Book save(final Book book) {
        HttpEntity<Object> requestEntity = createRequestEntity(book);
        ResponseEntity<Book> response = restTemplate.exchange(SAVE_ENDPOINT, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<Book>() {
                });
        return response.getBody();
    }

    public List<Book> delete(final Long id) {
        HttpEntity<Object> requestEntity = createRequestEntity(id);
        ResponseEntity<List<Book>> response = restTemplate.exchange(DELETE_ENDPOINT, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<List<Book>>() {
                });
        return response.getBody();
    }

    public Book find(final Long id) {
        return retrieve()
                .stream()
                .filter(a -> a.getIsbn().equals(id))
                .findFirst()
                .orElseGet(Book::new);
    }

    private HttpEntity<Object> createRequestEntity(final Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(id, headers);
    }

    private HttpEntity<Object> createRequestEntity(final Book book) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(book, headers);
    }

    private HttpEntity<Object> createRequestEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(null, headers);
    }

}
