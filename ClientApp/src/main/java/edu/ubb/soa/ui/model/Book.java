package edu.ubb.soa.ui.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Book {

    private Long isbn;

    @NotEmpty(message = "Text is required.")
    private String title;

    @NotEmpty(message = "Summary is required.")
    private String description;

    @NotNull(message = "No pages is required.")
    private Long noPages;

    @NotEmpty(message = "Author's name is required.")
    private String authorName;

    public Book() {
    }

    public Book(Long isbn, @NotEmpty(message = "Text is required.") String title, @NotEmpty(message = "Summary is required.") String description, @NotNull(message = "No pages is required.") Long noPages, @NotEmpty(message = "Author's name is required.") String authorName) {
        this.isbn = isbn;
        this.title = title;
        this.description = description;
        this.noPages = noPages;
        this.authorName = authorName;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getNoPages() {
        return noPages;
    }

    public void setNoPages(Long noPages) {
        this.noPages = noPages;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
