package edu.ubb.soa.ui.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import edu.ubb.soa.ui.model.Author;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AuthorService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String AUTHORS_PATH = "http://localhost:8082/authors";

    public List<Author> retrieveAuthors(final Author author) {
        HttpEntity<Object> requestEntity = createRequestEntity(author);
        ResponseEntity<List<Author>> response = restTemplate.exchange(AUTHORS_PATH, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<List<Author>>() {
                });
        return convertToLocalModel(response.getBody());
    }

    private List<Author> convertToLocalModel(List<Author> authorTransferObjects) {
        return Optional.ofNullable(authorTransferObjects)
                .map(list -> list.stream()
                        .map(a -> new Author(a.getFirstName(), a.getLastName(), a.getId()))
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    private HttpEntity<Object> createRequestEntity(Author author) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(author, headers);
    }

}
