package edu.ubb.soa.service;

import edu.ubb.soa.transfer.objects.AuthorTransferObject;
import edu.ubb.soa.model.Author;
import edu.ubb.soa.requests.AuthorsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class AuthorService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String AUTHORS_PATH = "http://localhost:8081/authors";

    public List<Author> retrieveAuthors(final String firstName, final String lastName) {
        HttpEntity<Object> requestEntity = createRequestEntity(firstName, lastName);
        try {
            ResponseEntity<List<AuthorTransferObject>> response = restTemplate.exchange(AUTHORS_PATH, HttpMethod.POST, requestEntity,
                    new ParameterizedTypeReference<List<AuthorTransferObject>>() {
                    });
            return convertToLocalModel(response.getBody());
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    private List<Author> convertToLocalModel(List<AuthorTransferObject> authorTransferObjects) {
        return Optional.ofNullable(authorTransferObjects)
                .map(list -> list.stream()
                        .map(a -> new Author(a.getFirstName(), a.getLastName(), a.getId()))
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    private HttpEntity<Object> createRequestEntity(String firstName, String lastName) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(new AuthorsRequest(firstName, lastName), headers);
    }

}
