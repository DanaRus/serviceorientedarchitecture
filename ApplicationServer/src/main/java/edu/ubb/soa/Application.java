package edu.ubb.soa;

import edu.ubb.soa.controller.PenguinPublishingController;
import edu.ubb.soa.model.Author;
import edu.ubb.soa.model.Title;
import edu.ubb.soa.requests.AuthorRequest;
import edu.ubb.soa.requests.TitleRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
public class Application {

    @Autowired
    private PenguinPublishingController penguinPublishingController;

    @PostMapping(path = "/authors", consumes = "application/json")
    public List<Author> authors(@RequestBody AuthorRequest request) {
        return penguinPublishingController.getAuthors(request.getFirstName(), request.getLastName());
    }

    @PostMapping(path = "/title", consumes = "application/json")
    public Title title(@RequestBody final TitleRequest request) {
        return penguinPublishingController.getTitle(request.getIsbn());
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
