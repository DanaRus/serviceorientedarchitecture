package edu.ubb.soa.model.penguin.publishing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "author")
@XmlAccessorType(XmlAccessType.FIELD)
public class AuthorDelegate {
    @XmlElement(name = "authorfirst")
    private String firstName;

    @XmlElement(name = "authorlast")
    private String lastName;

    @XmlElement(name = "titles")
    private ISBNS isbns;

    @XmlElement(name = "authorid")
    private Long id;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ISBNS getIsbns() {
        return isbns;
    }

    public void setIsbns(ISBNS isbns) {
        this.isbns = isbns;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}