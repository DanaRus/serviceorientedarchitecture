package edu.ubb.soa.delegate;

import edu.ubb.soa.model.penguin.publishing.AuthorDelegate;
import edu.ubb.soa.model.penguin.publishing.Authors;
import edu.ubb.soa.model.Title;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXB;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;

@Component
public class PenguinPublishingDelegate {

    private static final String AUTHORS_ENDPOINT = "https://reststop.randomhouse.com/resources/authors?lastName=%s&firstName=%s";

    private static final String TITLES_ENDPOINT = "https://reststop.randomhouse.com/resources/titles/%s";

    public List<AuthorDelegate> retrieveAuthors(final String firstName, final String lastName) {
        try {
            HttpURLConnection con = createConnection(String.format(AUTHORS_ENDPOINT, lastName, firstName));
            return JAXB.unmarshal(con.getInputStream(), Authors.class).getAuthors();
        } catch (Exception e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }

    public Title retrieveTitle(final String isbn) {
        try {
            HttpURLConnection con = createConnection(String.format(TITLES_ENDPOINT, isbn));
            return JAXB.unmarshal(con.getInputStream(), Title.class);
        } catch (Exception e) {
            e.printStackTrace();
            return new Title();
        }
    }

    private HttpURLConnection createConnection(final String urlAddress) throws IOException {
        URL url = new URL(urlAddress);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/xml");
        return con;
    }

}
