package edu.ubb.soa.model;

public class Book {

    private Long isbn;
    private String title;
    private String description;
    private Long noPages;
    private String authorName;

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getNoPages() {
        return noPages;
    }

    public void setNoPages(Long noPages) {
        this.noPages = noPages;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
