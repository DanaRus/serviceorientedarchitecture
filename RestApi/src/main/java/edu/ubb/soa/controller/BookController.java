package edu.ubb.soa.controller;

import edu.ubb.soa.model.Book;
import edu.ubb.soa.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public Iterable<Book> getBooks() {
        return bookService.findAll();
    }

    @PostMapping("/books/delete")
    public Iterable<Book> delete(@RequestBody Long id) {
        bookService.deleteBook(id);
        return bookService.findAll();
    }

    @PostMapping(name = "/books/create")
    public Book create(@RequestBody Book book) {
        return bookService.save(book);
    }

}
