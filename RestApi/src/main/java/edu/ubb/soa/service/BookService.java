package edu.ubb.soa.service;

import edu.ubb.soa.model.Book;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class BookService {

    private static AtomicLong counter = new AtomicLong();

    private final ConcurrentMap<Long, Book> books = new ConcurrentHashMap<>();

    public Iterable<Book> findAll() {
        return this.books.values();
    }

    public Book save(Book book) {
        Long id = book.getIsbn();
        if (id == null) {
            id = counter.incrementAndGet();
            book.setIsbn(id);
        }
        this.books.put(id, book);
        return book;
    }

    public void deleteBook(Long id) {
        this.books.remove(id);
    }
}
