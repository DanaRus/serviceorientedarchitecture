package edu.ubb.soa.ui.controller;

import edu.ubb.soa.ui.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import edu.ubb.soa.ui.model.Author;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/authors")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    List<Author> authorList = new ArrayList<>();

    @GetMapping
    public ModelAndView list() {
        return new ModelAndView("authors/list", "authors", authorList);
    }

    @GetMapping(name = "/authors", params = "form")
    public String createForm(@ModelAttribute Author author) {
        return "authors/searchForm";
    }

    @PostMapping(name = "/authors")
    public ModelAndView create(@Valid Author author, BindingResult result,
                               RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("authors/searchForm", "formErrors", result.getAllErrors());
        }
        authorList = authorService.retrieveAuthors(author);

        redirect.addFlashAttribute("globalMessage", "view.success");
        return new ModelAndView("redirect:/authors");
    }

}
