package edu.ubb.soa.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "title")
@XmlAccessorType(XmlAccessType.FIELD)
public class Title {
    @XmlElement(name = "titleweb")
    private String title;

    @XmlElement(name = "rgcopy")
    private String description;

    @XmlElement(name = "pages")
    private Long noPages;

    @XmlElement(name = "author")
    private String authorName;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getNoPages() {
        return noPages;
    }

    public void setNoPages(Long noPages) {
        this.noPages = noPages;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
