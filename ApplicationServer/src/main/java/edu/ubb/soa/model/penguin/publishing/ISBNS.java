package edu.ubb.soa.model.penguin.publishing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "titles")
@XmlAccessorType(XmlAccessType.FIELD)
public class ISBNS {
    @XmlElement(name = "isbn")
    private List<String> isbns;

    public List<String> getIsbns() {
        return isbns;
    }

    public void setIsbns(List<String> isbns) {
        this.isbns = isbns;
    }
}
