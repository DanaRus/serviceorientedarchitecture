package edu.ubb.soa.controller;

import edu.ubb.soa.delegate.PenguinPublishingDelegate;
import edu.ubb.soa.model.penguin.publishing.AuthorDelegate;
import edu.ubb.soa.model.Title;
import edu.ubb.soa.model.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class PenguinPublishingController {

    @Autowired
    private PenguinPublishingDelegate penguinPublishingDelegate;

    public List<Author> getAuthors(final String firstName, final String lastName) {
        return convertToLocalModel(penguinPublishingDelegate.retrieveAuthors(firstName, lastName));
    }

    private List<Author> convertToLocalModel(final List<AuthorDelegate> authors) {
        return authors.stream()
                .map(authorDelegate -> new Author(authorDelegate.getFirstName(), authorDelegate.getLastName(), authorDelegate.getIsbns().getIsbns(), authorDelegate.getId()))
                .collect(Collectors.toList());
    }

    public Title getTitle(final String isbn) {
        return penguinPublishingDelegate.retrieveTitle(isbn);
    }
}
