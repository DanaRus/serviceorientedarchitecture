package edu.ubb.soa.ui.controller;

import javax.validation.Valid;

import edu.ubb.soa.ui.model.Book;

import edu.ubb.soa.ui.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping(path = "/books")
public class BookController {

    @Autowired
    private BookService bookService;

    @GetMapping
    public ModelAndView list() {
        Iterable<Book> books = bookService.retrieve();
        return new ModelAndView("books/list", "books", books);
    }

    @GetMapping("/{id}")
    public ModelAndView view(@PathVariable("id") Long id) {
        Book book = bookService.find(id);
        return new ModelAndView("books/view", "book", book);
    }

    @GetMapping(name = "/books", params = "form")
    public String createForm(@ModelAttribute Book book) {
        return "books/form";
    }

    @PostMapping(name = "/books")
    public ModelAndView create(@Valid Book book, BindingResult result,
                               RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("books/form", "formErrors", result.getAllErrors());
        }
        final Book savedBook = bookService.save(book);
        redirect.addFlashAttribute("globalMessage", "view.success");
        return new ModelAndView("redirect:/books/{book.isbn}", "book.isbn", savedBook.getIsbn());
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        Iterable<Book> books = bookService.delete(id);
        return new ModelAndView("books/list", "books", books);
    }

    @GetMapping("modify/{id}")
    public ModelAndView modifyForm(@PathVariable("id") Long id) {
        Book book = bookService.find(id);
        return new ModelAndView("books/form", "book", book);
    }

}
