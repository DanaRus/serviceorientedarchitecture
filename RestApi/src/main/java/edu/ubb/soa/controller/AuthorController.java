package edu.ubb.soa.controller;

import edu.ubb.soa.model.Author;
import edu.ubb.soa.requests.AuthorsRequest;
import edu.ubb.soa.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @PostMapping(path = "/authors", consumes = "application/json")
    public List<Author> authors(@RequestBody final AuthorsRequest request) {
        return authorService.retrieveAuthors(request.getFirstName(), request.getLastName());
    }

}
