package edu.ubb.soa.model.penguin.publishing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "authors")
@XmlAccessorType(XmlAccessType.FIELD)
public class Authors {
    @XmlElement(name = "author")
    private List<AuthorDelegate> authors;

    public List<AuthorDelegate> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDelegate> authors) {
        this.authors = authors;
    }
}
