package edu.ubb.soa.model;

import java.util.List;

public class Author {
    private String firstName;
    private String lastName;
    private List<String> isbns;
    private Long id;

    public Author(String firstName, String lastName, List<String> isbns, Long id) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isbns = isbns;
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getIsbns() {
        return isbns;
    }

    public void setIsbns(List<String> isbns) {
        this.isbns = isbns;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
